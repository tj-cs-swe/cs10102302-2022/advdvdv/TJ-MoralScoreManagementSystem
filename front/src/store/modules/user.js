import {authenticate} from '../../api/index.js'
import { isValidJwt, EventBus } from '../../utils/index.js'

export default {
    namespace: true,
    state(){
      return {
        user:{},
        jwt:''
      };
    },
    mutations:{
      setUserData (state, payload) {
        console.log('setUserData payload = ', payload)
        state.userData = payload.userData
      },
      setJwtToken (state, payload) {
        console.log('setJwtToken payload = ', payload)
        localStorage.token = payload.jwt.token
        state.jwt = payload.jwt
      }
    },
    getters: {
      isAuthenticated(state){
        return isValidJwt(state.jwt.token);
      }
    },
    actions:{
      login(context, userData){
        context.commit('setUserData', {userData});
        return authenticate(userData)
        .then(response => {
          console.log('登陆成功');
          console.log(response.data);
          context.commit('setJwtToken', {jwt:response.data})
        })
        .catch(error=>{
          console.log('Error Authenticating: ', error);
        })
      }
    },
  };
  