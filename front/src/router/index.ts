import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import HomeView from "../views/HomeView.vue";
import Login from '../components/Login.vue';
import Base from '../components/Base.vue';
import Home from '../components/Home.vue';
import Search from '../components/Search.vue';
import Verify from '../components/Verify.vue';
import Activity from '../components/Activity.vue';
import ActivityFeedback from '../components/ActivityFeedback.vue';
import ActivityIssue from '../components/ActivityIssue.vue';
import ActivityModify from '../components/ActivityModify.vue';
import ActivitiesDetailed from '../components/ActivitiesDetailed.vue';
import ActivitiesVerify from '../components/ActivitiesVerify.vue';
import AdministratorLookUpScores from '../components/AdministratorLookUpScores.vue';
import ActivitiesDetailedAdministrator  from '../components/ActivitiesDetailedAdministrator.vue';
import ReviewActivity from '../components/ReviewActivity.vue'
import LeaveMessage from "../components/LeaveMessage.vue"
import PermissionList from "../components/PermissionList.vue"
import ChangePermission from "../components/ChangePermission.vue"
import UploadMessage from '../components/UploadMessage.vue'
import ModifyPass from '../components/ModifyPass.vue'

// import store from '../store'

const routes: Array<RouteRecordRaw> = [
  {
    path: "/activities_issue",
    component: ActivityIssue
  },
  {
    path: "/activities_modify",
    component: ActivityModify
  },
  {
    path: "/",
    redirect:"/login"
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/base",
    name: "base",
    component: Base
  },
  {
    path: "/profile",
    name:"profile",
    component: Home
  },
  {
    path: "/search",
    name: "search",
    component:Search
  },
  {
    path: "/verify",
    component: Verify
  },
  {
    path: "/activity",
    name: "activity",
    component:Activity
  },
  {
    path: "/activity_feedback",
    component:ActivityFeedback
  },
  {
    path: "/activities_detailed",
    component: ActivitiesDetailed
  },
  {
    path: "/activities_verify",
    component: ActivitiesVerify
  },
  {
    path: "/administrator_look_up_activities",
    component: AdministratorLookUpScores
  },
  {
    path: "/activities_detailed_administrator",
    component: ActivitiesDetailedAdministrator
  },
  {
    path: "/review_activity",
    component: ReviewActivity
  },
  {
    path: "/leave_message",
    component: LeaveMessage
  },
  {
    path: "/permission_list",
    component: PermissionList
  },
  {
    path: "/change_permission",
    component: ChangePermission
  },
  {
    path: "/upload_message",
    component: UploadMessage
  },
  {
    path: "/modify_pass",
    component: ModifyPass
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next)=>{
  console.log(to);
  console.log(from);
  let token=localStorage.getItem("token");
  if (to.path === '/login' || token) {
    next()
    console.log(token)
  } else {
    next('/login')
  }
})

export default router;
