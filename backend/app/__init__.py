from flask import Flask
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy

# from flask_login import LoginManager
from flask_cors import CORS
from flask_redis import FlaskRedis
from config import config
import os
from flask_migrate import Migrate

from .scheduler import APScheduler

mail = Mail()
db = SQLAlchemy()

# login_manager = LoginManager()
# login_manager.login_view = "user.login"

redis_client = FlaskRedis(decode_responses=True)

scheduler = APScheduler()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    CORS(app, supports_credentials=True)  # cors解决跨域

    mail.init_app(app)
    db.init_app(app)
    # login_manager.init_app(app)
    redis_client.init_app(app)

    # 初始化定时器
    scheduler.init_app(app)
    # 启动定时器，默认后台启动了
    scheduler.start()

    from .main import main as main_blueprint

    app.register_blueprint(main_blueprint, url_prefix="/api")

    from .user import user as user_blueprint

    app.register_blueprint(user_blueprint, url_prefix="/api/user")

    from .browse import browse as browse_blueprint

    app.register_blueprint(browse_blueprint, url_prefix="/api/browse")

    from .manage import manage as manage_blueprint

    app.register_blueprint(manage_blueprint, url_prefix="/api/manage")

    from .feedbackReview import feedbackReview as feedbackReview_blueprint

    app.register_blueprint(feedbackReview_blueprint, url_prefix="/api/feedbackReview")

    from .notice import notice as notice_blueprint

    app.register_blueprint(notice_blueprint, url_prefix="/api/notice")

    from .authority import authority as authority_blueprint

    app.register_blueprint(authority_blueprint, url_prefix="/api/authority")

    return app


app = create_app(os.getenv("FLASK_CONFIG") or "default")
migrate = Migrate(app, db)

from app import commands
