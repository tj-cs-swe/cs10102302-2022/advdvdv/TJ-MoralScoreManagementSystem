from flask import jsonify, current_app, request
from datetime import datetime
from . import notice
from .. import db
from ..models import Notice, User
from ..response import RET
from ..email import send_email
from .utilities import brief_user
from ..decorator import token_required, admin_required

# from .utilities import notice_to_dict
# from sqlalchemy import and_


@notice.route("/newnum", methods=["POST"])
@token_required
def newnum(current_user):
    user = current_user
    # print(user.id)
    try:
        new_num = (
            Notice.query.filter(Notice.user_id == user.id, Notice.checked == 0)
            .filter_by()
            .count()
        )
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="消息数查询失败")
    # print(new_num)
    return jsonify(code=RET.OK, msg="消息数查询成功", num=new_num)


@notice.route("/newall", methods=["POST"])
@token_required
def newall(current_user):
    user = current_user
    # print(user.id)
    try:
        notices = (
            Notice.query.filter(Notice.user_id == user.id, Notice.checked == 0)
            .filter_by()
            .all()
        )
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="消息数查询失败")
    return jsonify(code=RET.OK, msg="消息数查询成功", notices=list(map(dict, notices)))


@notice.route("/message", methods=["POST"])
@token_required
@admin_required
def message(current_user):
    user = current_user
    if user.author == 1:
        data = request.get_json()
        receiver_list = data.get("receiver_list")
        print(receiver_list)
        content = data.get("content")
        for receiver_info in receiver_list:
            print(receiver_info)
            receiver = User.query.filter_by(id=receiver_info["id"]).first()
            send_email(
                receiver.email,
                "留言通知",
                "mail/message",
                username=receiver.username,
                time=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            )
            try:
                notice = Notice(
                    user_id=receiver.id,
                    notice_type=5,
                    content=content,
                    notice_time=datetime.now(),
                )
                db.session.add(notice)
                db.session.commit()
            except Exception as e:
                current_app.logger.debug(e)
                db.session.rollback()  # commit失败需要回滚
        return jsonify(code=RET.OK, msg="发送留言通知成功")
    else:
        return jsonify(code=RET.AUTHERR, msg="当前用户不是管理员，没有发送留言通知的权限")


@notice.route("/upload_message", methods=["POST"])
@token_required
@admin_required
def upload_message(current_user):
    try:
        users = User.query.all()
        print(type(users))
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="查询所有用户失败")
    return jsonify(
        code=RET.OK,
        msg="查询所有用户成功",
        studentNumber=len(users),
        allStudents=list(map(brief_user, users)),
    )


# @notice.route("/registreminder", methods=["POST"])
# @login_required
# def registreminder():
#     data = request.get_json()
#     topic = data.get("topic")
#     content1="您当前德育分未满60，有新的德育活动发布啦！"
#     content2="您当前德育分未满80，有新的德育活动发布啦！"
#     try:
#         users60=User.query.filter(User.score<60).all()
#         users6080=User.query.filter(User.score>=60,User.score<80).all()
#         users80=User.query.filter(User.score>=80).all()

#     except Exception as e:
#         current_app.logger.debug(e)
#         return jsonify(code=RET.DBERR, msg="查询未满60分和已满60分但未满80分的用户失败")

#     for u in users60:
#         #print(u)
#         #发送邮件提醒
#         send_email(
#             u.email,
#             "德育活动报名提醒",
#             "mail/registration_reminder_60",
#             username=u.username,
#             score=u.score,
#             topic=topic,
#             time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
#         )

#         #系统内通知提醒
#         try:
#             notice = Notice(user_id=u.id, notice_type=2, content=content1,notice_time=datetime.now())
#             db.session.add(notice)
#             db.session.commit()
#         except Exception as e:
#             current_app.logger.debug(e)
#             db.session.rollback()  # commit失败需要回滚

#     for u in users6080:
#         #print(u)
#         send_email(
#             u.email,
#             "德育活动报名提醒",
#             "mail/registration_reminder_6080",
#             username=u.username,
#             score=u.score,
#             topic=topic,
#             time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
#         )
#         #系统内通知提醒
#         try:
#             notice = Notice(user_id=u.id, notice_type=2, content=content1,notice_time=datetime.now())
#             db.session.add(notice)
#             db.session.commit()
#         except Exception as e:
#             current_app.logger.debug(e)
#             db.session.rollback()  # commit失败需要回滚
#     #print(users60,users6080,users80)
#     return jsonify(code=RET.OK, msg="德育活动报名提醒成功")

