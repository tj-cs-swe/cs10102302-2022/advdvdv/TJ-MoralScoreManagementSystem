from ..models import User, Activity


def brief_user(activity):
    keys = ("id", "username")
    ret = {}
    for key in keys:
        ret[key] = activity.__getitem__(key)
    return ret
