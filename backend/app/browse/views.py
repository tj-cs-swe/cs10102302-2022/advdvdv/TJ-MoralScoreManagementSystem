from flask import jsonify, request, current_app
from . import browse
from .. import db
from ..models import User, Activity, Participate, TempRight
from ..response import RET
from ..decorator import token_required, admin_required
from .utilities import (
    act_brief_dict,
    # paginate_to_dict,
    get_user_score,
    participant_to_dict,
    get_act_info,
    act_with_author,
    activity_detail,
)
from datetime import datetime
from sqlalchemy import and_, or_


@browse.route("/all", methods=["POST"])
@token_required
def all_activities(current_user):
    data = request.get_json()
    print(data)
    page = data.get("page", 1)
    per_page = data.get("per_page", 5)
    activity_status_type = data.get("activity_status_type", 0)
    searching_keyword = data.get("searching_keyword", "")

    try:
        if activity_status_type == 0:
            paginate = (
                Activity.query.filter(
                    or_(
                        Activity.topic.like("%" + searching_keyword + "%"),
                        Activity.content.like("%" + searching_keyword + "%"),
                    )
                )
                .order_by(Activity.startTime)
                .paginate(page, per_page, error_out=True)
            )
            activities = paginate.items
            total = paginate.total
        elif activity_status_type == 1:
            paginate = (
                Activity.query.filter(
                    and_(
                        Activity.startTime > datetime.now(),
                        or_(
                            Activity.topic.like("%" + searching_keyword + "%"),
                            Activity.content.like("%" + searching_keyword + "%"),
                        ),
                    )
                )
                .order_by(Activity.startTime)
                .paginate(page, per_page, error_out=True)
            )
            activities = paginate.items
            total = paginate.total
        elif activity_status_type == 2:
            start = (page - 1) * per_page
            end = (page - 1) * per_page + per_page
            activities = [
                activity.activity
                for activity in current_user.activities
                if activity.state == 2
                and (
                    searching_keyword in activity.activity.topic
                    or searching_keyword in activity.activity.content
                )
            ]
            total = len(activities)
            activities = activities[start:end]
        elif activity_status_type == 3:
            start = (page - 1) * per_page
            end = (page - 1) * per_page + per_page
            activities = [
                activity.activity
                for activity in current_user.activities
                if activity.state == 1
                and (
                    searching_keyword in activity.activity.topic
                    or searching_keyword in activity.activity.content
                )
            ]
            total = len(current_user.activities)
            activities = activities[start:end]
        elif activity_status_type == 4:
            paginate = (
                Activity.query.filter(
                    and_(
                        Activity.endTime < datetime.now(),
                        or_(
                            Activity.topic.like("%" + searching_keyword + "%"),
                            Activity.content.like("%" + searching_keyword + "%"),
                        ),
                    )
                )
                .order_by(Activity.startTime)
                .paginate(page, per_page, error_out=True)
            )
            activities = paginate.items
            total = paginate.total

    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库查询错误")

    # print(list(map(dict, activities)))  # debug
    temprights = TempRight.query.filter_by(user_id=current_user.id).all()
    temp_ids = [tempright.activity_id for tempright in temprights]
    activities_dict = [
        act_with_author(activity, current_user, temp_ids) for activity in activities
    ]  # list(map(act_with_author, activities))

    return jsonify(
        code=RET.OK, msg="获取全部活动成功", activities=activities_dict, total=total,
    )


@browse.route("/details/<act_id>", methods=["POST"])
@token_required
def activity_detals(current_user, act_id):
    try:
        activity = Activity.query.filter_by(id=act_id).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库查询错误")

    activity_dict = activity_detail(activity, current_user)
    # print(dict(activity))  # debug

    return jsonify(code=RET.OK, msg="获取活动详情成功", activity=activity_dict)


@browse.route("/score-by-no", methods=["POST"])
@token_required
@admin_required
def score_by_no(current_user):
    data = request.get_json()
    no = data.get("no")
    # if not all([no]):
    #     return jsonify(code=RET.PARAMERR, msg="参数不完整")
    try:
        us = User.query.filter(User.no.like("%" + no + "%")).all()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="用户查询失败")
    return jsonify(code=RET.OK, msg="查询德育分成功", scores=list(map(get_user_score, us)))


@browse.route("/participants-by-act", methods=["POST"])
@token_required
@admin_required
def participants_by_act(current_user):
    data = request.get_json()
    activity_id = data.get("activity_id")
    if not all([activity_id]):
        return jsonify(code=RET.PARAMERR, msg="参数不完整")
    try:
        # participants = (
        #     db.session.query(User.id, User.no, User.username, Participate.score)
        #     .filter(Participate.activity_id == activity_id)
        #     .filter(Participate.user_id == User.id)
        #     .all()
        # )
        activity = Activity.query.filter_by(id=activity_id).first()
        participants = activity.users
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="参与者查询失败")

    # print(participants)  # debug
    return jsonify(
        code=RET.OK,
        msg="查询参与者和得分成功",
        participants=list(map(participant_to_dict, participants)),
    )


@browse.route("/checkscore", methods=["POST"])
@token_required
def checkscore(current_user):
    # data = request.get_json()
    user = current_user
    try:
        score_total = user.score
        activities = user.activities
        # print((activities))
        # for activity in activities:
        #     print(activity.state)
        #     print(activity.activity.id)
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="查询个人德育分失败")
    return jsonify(
        code=RET.OK,
        msg="查询个人德育分成功",
        score_total=score_total,
        activities=list(map(get_act_info, activities)),
    )

