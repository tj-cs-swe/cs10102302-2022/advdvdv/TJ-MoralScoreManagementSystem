from ..models import User, Activity, Participate
from sqlalchemy import and_


def act_brief_dict(activity):
    keys = ("id", "topic", "content", "startTime", "endTime", "college")
    ret = {}
    for key in keys:
        if key == "content":
            ret[key] = activity.__getitem__(key)[0:20] + "..."  # 暂时写死前20个字符
        else:
            ret[key] = activity.__getitem__(key)
    return ret


# def paginate_to_dict(paginate):
#     ret = {}
#     ret["page"] = paginate.page
#     ret["pages"] = paginate.pages
#     ret["total"] = paginate.total
#     return ret


def get_user_score(user):
    ret = {}
    ret["id"] = user.id
    ret["no"] = user.no
    ret["username"] = user.username
    ret["email"] = user.email
    ret["score"] = user.score
    return ret


def participant_to_dict(participant):
    # keys = ("user_id", "no", "username", "score")
    # return dict(zip(keys, participant))
    keys = ("id", "no", "username")
    ret = {}
    for key in keys:
        if key == "score":
            ret[key] = participant.score
        else:
            ret[key] = participant.user.__getitem__(key)
    return ret


# 用于在个人德育分查询中查看已参加过的活动情况
def get_act_info(activity):
    keys = ("id", "topic", "startTime", "endTime", "score", "state")
    ret = {}
    for key in keys:
        if key == "state":
            ret[key] = activity.state
        else:
            ret[key] = activity.activity.__getitem__(key)
    return ret


def act_with_author(activity, current_user, temp_ids):
    ret = {}
    ret["id"] = activity.id
    ret["topic"] = activity.topic
    ret["startTime"] = activity.startTime.strftime("%Y-%m-%d %H:%M:%S")
    ret["endTime"] = activity.endTime.strftime("%Y-%m-%d %H:%M:%S")
    ret["academy"] = activity.college.name
    ret["college_id"] = activity.college_id
    ret["score"] = activity.score
    ret["maximum"] = activity.maximum
    ret["content"] = activity.content
    ret["author"] = 1 if activity.id in temp_ids else 0
    parti = Participate.query.filter(
        and_(
            Participate.activity_id == activity.id,
            Participate.user_id == current_user.id,
        )
    ).first()
    ret["state"] = -1 if parti is None else parti.state

    return ret


def activity_detail(activity, current_user):
    ret = {}
    ret["id"] = activity.id
    ret["topic"] = activity.topic
    ret["startTime"] = activity.startTime.strftime("%Y-%m-%d %H:%M:%S")
    ret["endTime"] = activity.endTime.strftime("%Y-%m-%d %H:%M:%S")
    ret["academy"] = activity.college.name
    ret["college_id"] = activity.college_id
    ret["score"] = activity.score
    ret["maximum"] = activity.maximum
    ret["content"] = activity.content
    ret["parti_num"] = len(activity.users)

    parti = Participate.query.filter(
        and_(
            Participate.activity_id == activity.id,
            Participate.user_id == current_user.id,
        )
    ).first()
    ret["state"] = -1 if parti is None else parti.state

    return ret
