from flask import jsonify, request, current_app, make_response
import os
from . import feedbackReview
from .. import db
from ..models import User, Activity, Participate, Feedback, Notice
from ..response import RET
from ..email import send_email
from .utilities import feedback_brief_dict, feedback_detail
from datetime import datetime
from ..decorator import admin_required, token_required


@feedbackReview.route("/pageesle", methods=["POST"])
@token_required
@admin_required
def pageesle(current_user):
    data = request.get_json()
    page = data.get("page", 1)
    per_page = data.get("per_page", 5)
    try:
        feedbacks = Feedback.query.order_by(Feedback.feedback_time).paginate(page, per_page, error_out=True)
        feedbacks=feedbacks.items
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="查询反馈错误")
    if feedbacks is None:
        return jsonify(code=RET.DBERR, msg="不存在反馈信息")
    return jsonify(
        code=RET.OK, msg="查询所有反馈成功", info=list(map(feedback_brief_dict, feedbacks))
    )


@feedbackReview.route("/detail", methods=["POST"])
@token_required
@admin_required
def detail(current_user):
    data = request.get_json()
    feedback_id = data.get("id")
    try:
        feedback = Feedback.query.filter_by(id=feedback_id).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="查询反馈详细信息错误")
    if feedback is None:
        return jsonify(code=RET.DBERR, msg="该反馈信息不存在")
    return jsonify(code=RET.OK, msg="查询反馈详细信息成功", info=feedback_detail(feedback))


@feedbackReview.route("/pic", methods=["POST"])
@token_required
def pic(current_user):
    data = request.get_json()
    pic_name = data.get("pic")

    file_dir = os.path.join(os.getcwd(), "app/files/img")
    pic_path = os.path.join(file_dir, pic_name)
    try:
        image_data = open(pic_path, "rb").read()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="读取图片文件错误")

    response = make_response(image_data)
    response.headers["Content-Type"] = "image/jpeg"  # 返回的内容类型必须修改
    return response


# 提交审核
@feedbackReview.route("/submitexamine", methods=["POST"])
@token_required
@admin_required
def submitexamine(current_user):
    data = request.get_json()
    feedback_id = data.get("feedback_id")
    is_pass = data.get("is_pass")
    reason = data.get("reason")
    try:
        feedback = Feedback.query.filter_by(id=feedback_id).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="查询反馈错误")

    uid = feedback.user_id
    aid = feedback.activity_id
    thisu = User.query.filter_by(id=uid).first()
    activity = Activity.query.filter_by(id=aid).first()
    filterList = [Participate.user_id == uid, Participate.activity_id == aid]
    par = Participate.query.filter(*filterList).first()
    if is_pass:
        try:
            n = Notice(
                user_id=uid,
                notice_type=1,
                content=activity.topic + "审核通过",
                notice_time=datetime.now(),
            )
            db.session.add_all([n])
            thisu.score = thisu.score + activity.score
            par.state = 2
            db.session.commit()
        except Exception as e:
            current_app.logger.debug(e)
            return jsonify(code=RET.DBERR, msg="数据库错误")

        return jsonify({"code": RET.OK, "msg": "审核通过"})

    try:
        par.state = 3
        n = Notice(
            user_id=uid,
            notice_type=3,
            content=activity.topic + "审核不通过,原因是：" + reason,
            notice_time=datetime.now(),
        )
        db.session.add_all([n])
        db.session.commit()
    except Exception as e:
        current_app.logger.debug(e)
        jsonify(code=RET.DBERR, msg="数据库错误")

    send_email(
        thisu.email,
        "审核结果通知",
        "mail/examina_fail",
        username=thisu.username,
        theme=activity.topic,
        reason=reason,
        time=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
    )

    return jsonify({"code": RET.OK, "msg": "审核不通过"})
