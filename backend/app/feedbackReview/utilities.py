from ..models import User, Activity, Feedback
import os


def feedback_brief_dict(feedback):
    keys = (
        "id",
        "user_name",
        "activity_topic",
        "location",
        "text",
        "feedback_time",
        "pic",
        "score",
        "is_copy",
    )
    ret = {}

    user_id = feedback.user_id
    user = User.query.filter_by(id=user_id).first()

    activity_id = feedback.user_id
    activity = Activity.query.filter_by(id=activity_id).first()

    # file_dir = os.path.join(os.getcwd(), "app/files/img")
    # # if not os.path.exists(file_dir):
    # #     os.makedirs(file_dir)

    for key in keys:
        if key == "user_name":
            ret[key] = user.__getitem__("username")
        elif key == "activity_topic":
            ret[key] = activity.__getitem__("topic")
        # elif key=="pic":
        #     pic_name=feedback.pic
        #     pic_path = os.path.join(file_dir, pic_name)
        #     image_data = open(pic_path, "rb").read()
        #     # response = make_response(image_data)
        #     # response.headers['Content-Type'] = 'image/png'#返回的内容类型必须修改
        #     ret[key]=image_data
        elif key == "score":
            ret[key] = activity.__getitem__("score")
        else:
            ret[key] = feedback.__getitem__(key)
        # print (ret)
    return ret


def feedback_detail(feedback):
    keys = (
        "activity_id",
        "activity_topic",
        "score",
        "user_name",
        "user_no",
        "text",
        "is_copy",
        "location",
        "pic",

    )
    ret = {}

    user_id = feedback.user_id
    user = User.query.filter_by(id=user_id).first()

    activity_id = feedback.user_id
    activity = Activity.query.filter_by(id=activity_id).first()
    for key in keys:
        if key == "user_name":
            ret[key] = user.__getitem__("username")
        elif key == "user_no":
            ret[key] = user.__getitem__("no")
        elif key == "activity_topic":
            ret[key] = activity.__getitem__("topic")
        elif key == "score":
            ret[key] = activity.__getitem__("score")
        elif key == "activity_id":
            ret[key] = activity.__getitem__("id")
        else:
            ret[key] = feedback.__getitem__(key)
        # print (ret)
    return ret
