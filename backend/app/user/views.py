import random
from datetime import datetime, timedelta
from flask import jsonify, request, current_app, make_response
from . import user
from .. import db, redis_client
from ..models import User, College
from ..email import send_email, validate_email
from ..response import RET
from ..decorator import token_required
import jwt
import hashlib
from .avatar import create_font_img
import os


@user.route("/login", methods=["POST"])
def login():
    data = request.get_json()
    print(data)  # debug
    email = data.get("email")
    password = data.get("password")
    if not all([email, password]):
        return jsonify(code=RET.PARAMERR, msg="参数不完整")
    try:
        u = User.query.filter_by(email=email.lower()).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="用户查询失败")
    if u is not None and u.verify_password(password):
        # token
        token = jwt.encode(
            {
                "sub": u.id,
                "iat": datetime.utcnow(),
                "exp": datetime.utcnow() + timedelta(days=1),
            },
            current_app.config["SECRET_KEY"],
            algorithm="HS256",
        )
        # print("token=", token)
        return jsonify(code=RET.OK, msg="用户登录成功", token=token, author=u.author)
    return jsonify(code=RET.LOGINERR, msg="邮箱或密码错误")


# 使用token其实是可以不用后端处理登出的
@user.route("/logout", methods=["POST"])
@token_required
def logout(current_user):
    return jsonify(code=RET.OK, msg="用户注销成功！")


@user.route("/mailcode", methods=["POST"])
def send_mail_code():
    data = request.get_json()
    print(data)
    email = data.get("email")
    username = data.get("name")
    if not all([email, username]):
        return jsonify(code=RET.PARAMERR, msg="请填写用户名和邮箱")
    if not validate_email(email):
        return jsonify(code=RET.PARAMERR, msg="邮箱格式不正确")
    try:
        user_email = User.query.filter_by(email=email).first()
        user_username = User.query.filter_by(username=username).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库查询错误")
    if user_email:
        return jsonify(code=RET.DATAEXIST, msg="该邮箱已被注册")
    if user_username:
        return jsonify(code=RET.DATAEXIST, msg="该用户名已被注册")
    email_code = "%08d" % random.randint(0, 99999999)
    current_app.logger.debug("邮箱验证码为: " + email_code)
    # redis逻辑
    try:
        redis_client.set("AUTHCODE:" + email, email_code, 300)
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="存储邮箱验证码失败")
    # 发送邮件
    send_email(
        email,
        "请查收验证码",
        "mail/send_code",
        username=username,
        email_code=email_code,
        info="注册",
        time=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
    )

    return jsonify(code=RET.OK, msg="验证码发送成功")


@user.route("/register", methods=["POST"])
def register():
    data = request.get_json()
    print(data)
    email = data.get("email")
    username = data.get("name")
    tel = data.get("mobile")
    authcode_client = data.get("authcode")
    password = data.get("password")
    if not all([email, username, authcode_client, password]):
        return jsonify(code=RET.PARAMERR, msg="参数不完整")
    # 处理验证码
    try:
        authcode_server = redis_client.get("AUTHCODE:" + email)
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="验证码查询失败")
    if authcode_client != authcode_server:
        current_app.logger.debug("用户输入的验证码为：" + authcode_client)
        return jsonify(code=RET.PARAMERR, msg="验证码错误")

    u = User(email=email, tel=tel, username=username, password=password)
    try:
        db.session.add(u)
        db.session.commit()
    except Exception as e:
        current_app.logger.debug(e)
        db.session.rollback()  # commit失败需要回滚
        return jsonify(code=RET.DBERR, msg="注册失败")
    return jsonify(code=RET.OK, msg="用户注册成功")


@user.route("/forgetpwd", methods=["POST"])
def forgetpwd():
    data = request.get_json()
    email = data.get("email")

    try:
        u = User.query.filter_by(email=email).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库查询错误")

    if u:
        email_code = "%08d" % random.randint(0, 99999999)
        current_app.logger.debug("邮箱验证码为: " + email_code)
        # redis逻辑
        try:
            redis_client.set("AUTHCODE:" + email, email_code, 60)
        except Exception as e:
            current_app.logger.debug(e)
            return jsonify(code=RET.DBERR, msg="存储邮箱验证码失败")
        # 发送邮件
        send_email(
            email,
            "请查收验证码",
            "mail/send_code",
            username=u.username,
            email_code=email_code,
            info="修改密码",
            time=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        )
        return jsonify(code=RET.OK, msg="验证码发送成功")
    else:
        return jsonify(code=RET.DATANOTEXIST, msg="邮箱不存在")


@user.route("/modifypwd", methods=["POST"])
@token_required
def modifypwd(current_user):
    data = request.get_json()
    prePassword = data.get("prePassword")
    newPassword = data.get("newPassword")
    u = current_user
    if u.verify_password(prePassword):
        u.password = newPassword
        try:
            db.session.commit()
            return jsonify({"code": RET.OK, "msg": "修改密码成功"})
        except Exception as e:
            current_app.logger.debug(e)
            db.session.rollback()  # commit失败需要回滚
            return jsonify({"code": RET.DBERR, "msg": "数据库查询错误"})
    else:
        return jsonify({"code": RET.PWDERR, "msg": "原密码错误"})


@user.route("/setpwd", methods=["POST"])
def setpwd():
    data = request.get_json()
    email = data.get("email")
    password = data.get("password")
    authcode = data.get("authcode")

    u = User.query.filter_by(email=email).first()
    try:
        authcode_server = redis_client.get("AUTHCODE:" + email)
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="验证码查询失败")

    if u:
        if authcode_server == authcode:
            u.password = password
            try:
                db.session.commit()
                return jsonify({"code": RET.OK, "msg": "修改密码成功"})
            except Exception as e:
                current_app.logger.debug(e)
                db.session.rollback()  # commit失败需要回滚
        else:
            return jsonify(code=RET.PARAMERR, msg="验证码错误")
    return jsonify({"code": RET.DATANOTEXIST, "msg": "邮箱不存在或者其他错误"})


@user.route("/bind", methods=["POST"])
@token_required
def bind(current_user):
    form = request.get_json()
    print(form)
    current_user.no = form.get("number")
    bindCode = form.get("bindCode")
    if current_user.college_id is not None:
        return jsonify(code=RET.DBERR, msg="当前用户已绑定学院")
    try:
        college = College.query.filter_by(bindCode=bindCode).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="根据绑定码查找学院失败")
    if college:
        current_user.college_id = college.id
        # current_user.bindCode = form.get("bindCode")
        try:
            db.session.add(current_user)
            db.session.commit()
        except Exception as e:
            current_app.logger.debug(e)
            db.session.rollback()  # commit失败需要回滚
            return jsonify(code=RET.DBERR, msg="绑定失败")
    else:
        return jsonify(code=RET.DBERR, msg="绑定码所对应的学院不存在")
    return jsonify(code=RET.OK, msg="绑定成功")


@user.route("/avatar", methods=["POST"])
@token_required
def avatar(current_user):
    uid = request.get_json().get("uid")
    name = ""
    if uid is None:
        name = current_user.username
    else:
        name = User.query.filter_by(id=uid).first().username

    try:
        create_font_img(name[0], "name", "new.jpg")
        os.remove("new.jpg")
        image_data = open("name_font.png", "rb").read()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="图片错误")

    response = make_response(image_data)
    response.headers["Content-Type"] = "image/jpeg"  # 返回的内容类型必须修改
    os.remove("name_font.png")
    return response


@user.route("/profile", methods=["POST"])
@token_required
def profile(current_user):
    return jsonify(
        code=RET.OK,
        msg="返回信息成功",
        data={
            "name": current_user.username,
            "academy": current_user.college.name,
            "author": current_user.author,
        },
    )


@user.route("/role", methods=["POST"])
@token_required
def role(current_user):
    return jsonify(code=RET.OK, msg="返回信息成功", data={"author": current_user.author,},)

