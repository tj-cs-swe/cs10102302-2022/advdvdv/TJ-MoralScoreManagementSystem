import cv2
import numpy as np
from PIL import ImageFont, ImageDraw, Image
import random


def new_image(size, color, name):
    img = Image.new("RGB", size, color)  # 生成图片
    # img.show()  # 展示图片
    img.save(name)  # 保存图片


def create_font_img(value, file_name, path):
    new_image((64, 64), (100, 100, 100,), "new.jpg")
    # new_image((64, 64),
    #           (100 + random.randint(0, 80), 100 + random.randint(0, 80), 100 + random.randint(0, 80),),
    #           "new.jpg")
    img = cv2.imread(path)  # 打开底片
    font_path = "Font/mysimsun.ttc"  # 设置需要显示的字体
    font = ImageFont.truetype(font_path, size=50)  # 字体大小为 50
    img_pil = Image.fromarray(img)
    draw = ImageDraw.Draw(img_pil)
    text_width, text_height = draw.textsize(value, font)  # 获取字体宽高
    # 绘制文字信息，img.shape 求图片的宽和高，字体绘制的开始位置（x, y），(255,255,255) 为白色字体
    draw.text(
        ((img.shape[1] - text_width) / 2, (img.shape[0] - text_height) / 2),
        value,
        fill=(255, 255, 255),
        font=font,
    )
    bk_img = np.array(img_pil)
    # cv2.imshow("add_text", bk_img)
    # cv2.waitKey()
    cv2.imwrite(file_name + "_font.png", bk_img)
