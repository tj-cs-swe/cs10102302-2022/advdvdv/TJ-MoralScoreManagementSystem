from werkzeug.security import generate_password_hash, check_password_hash
from . import db

# from flask_login import UserMixin
# , login_manager


class User(db.Model):  # UserMixin,
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    tel = db.Column(db.String(32), unique=True)
    username = db.Column(db.String(32), unique=True)
    no = db.Column(db.String(16))
    password_hash = db.Column(db.String(128))
    score = db.Column(db.SmallInteger, default=0)
    # bindCode = db.Column(db.String(16))  # 暂时用学院码代替学院
    college_id = db.Column(db.SmallInteger, db.ForeignKey("colleges.id"), default=0)

    # 1为管理员；2为辅导员；3为普通用户
    author = db.Column(db.SmallInteger, nullable=False, default=3)

    # activities=db.relationship('Activity',secondary=Participate,backref=db.backref('users',lazy="dynamic"),lazy="dynamic")
    activities = db.relationship("Participate", back_populates="user")

    @property
    def password(self):
        raise AttributeError("password is not a readable attribute")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def keys(self):
        return (
            "id",
            "email",
            "tel",
            "username",
            "role_id",
            "no",
            "password_hash",
            "score",
            "bindCode",
            "college_id",
        )

    def __getitem__(self, item):
        return getattr(self, item)

    def __repr__(self):
        return "<User %r>" % self.username


class College(db.Model):
    __tablename__ = "colleges"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), unique=True)
    bindCode = db.Column(db.String(16), unique=True)

    users = db.relationship("User", backref="college", lazy="dynamic")
    activities = db.relationship("Activity", backref="college", lazy="dynamic")

    def __getitem__(self, item):
        return getattr(self, item)

    def __repr__(self):
        return "<College %r>" % self.name


class Activity(db.Model):
    __tablename__ = "activities"
    id = db.Column(db.Integer, primary_key=True)
    topic = db.Column(db.String(64))
    content = db.Column(db.Text)
    startTime = db.Column(db.DateTime)
    endTime = db.Column(db.DateTime)
    college_id = db.Column(db.SmallInteger, db.ForeignKey("colleges.id"))

    score = db.Column(db.SmallInteger)  # 本次参加活动获得的德育分
    maximum = db.Column(db.Integer)  # 德育活动允许的人数上限
    # state=db.Column(db.String(64))
    users = db.relationship("Participate", back_populates="activity")

    # users = db.relationship("Participate",backref=db.backref('activity') )

    def keys(self):
        return (
            "id",
            "topic",
            "content",
            "startTime",
            "endTime",
            "college_id",
            "score",
            "maximum",
        )

    def __getitem__(self, item):
        return getattr(self, item)

    def __repr__(self):
        return "<Activity %r>" % self.topic


class Participate(db.Model):
    __tablename__ = "participates"
    # id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    # user_id = db.Column(db.Integer, db.ForeignKey("users.id"), primary_key=True)
    # activity_id = db.Column(
    #     db.Integer, db.ForeignKey("activities.id"), primary_key=True
    # )
    # id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.ForeignKey("users.id"), primary_key=True)
    activity_id = db.Column(db.ForeignKey("activities.id"), primary_key=True)
    # score = db.Column(db.SmallInteger, default=0)  # 本次参加活动获得的德育分
    state = db.Column(db.SmallInteger)  ### 0、1、2，3分别代表未审核、正在审核、已审核成功和已审核失败

    user = db.relationship("User", back_populates="activities")
    activity = db.relationship("Activity", back_populates="users")

    # feedback = db.relationship("Feedback", back_populates="feed_act")

    # TODO: 是否需要报名时间等信息？

    def __repr__(self):
        return "<Participate %r>" % str(self.user_id) + str(self.activity_id)


class Feedback(db.Model):
    __tablename__ = "feedbacks"
    # user_id = db.Column(db.ForeignKey("users.id"), primary_key=True)
    # activity_id = db.Column(db.ForeignKey("activities.id"), primary_key=True)
    # location = db.Column(db.String(128))

    # user = db.relationship("User", back_populates="activities")
    # activity = db.relationship("Activity", back_populates="users")

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.ForeignKey("users.id"), nullable=False)
    activity_id = db.Column(db.ForeignKey("activities.id"), nullable=False)

    location = db.Column(db.String(128), nullable=False)
    text = db.Column(db.Text(256), nullable=False)
    feedback_time = db.Column(db.DateTime, nullable=False)

    pic = db.Column(db.String(16))
    is_copy = db.Column(db.Boolean, default=False)

    def __getitem__(self, item):
        return getattr(self, item)

    def __repr__(self):
        return "<Feedback %r>" % str(self.id)


class Notice(db.Model):
    __tablename__ = "notices"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.ForeignKey("users.id"), nullable=False)
    notice_type = db.Column(db.SmallInteger, nullable=False)
    # 1:德育活动反馈提醒
    # 2:德育活动报名提醒
    # 3:审核结果提醒
    # 4:提醒审核员去审核
    # 5：留言通知
    content = db.Column(db.Text)
    notice_time = db.Column(db.DateTime, nullable=False)
    checked = db.Column(db.Boolean, default=False)

    def keys(self):
        return ("id", "user_id", "notice_type", "content", "notice_time", "checked")

    def __getitem__(self, item):
        return getattr(self, item)

    def __repr__(self):
        return "<Notice %r>" % str(self.id)


class TempRight(db.Model):
    __tablename__ = "temprights"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.ForeignKey("users.id"), nullable=False)
    activity_id = db.Column(db.ForeignKey("activities.id"), nullable=False)

    user = db.relationship("User", backref="tempright")
    activity = db.relationship("Activity", backref="tempright")

    def __repr__(self):
        return "<TempRight %r>" % str(self.id)


# @login_manager.user_loader
# def load_user(user_id):
#     return User.query.get(int(user_id))
