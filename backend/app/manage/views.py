from flask import jsonify, request, current_app
from ..response import RET
from .. import db, scheduler
from ..models import User, Activity, Participate, Feedback, TempRight, College
from . import manage

from ..response import RET
from datetime import datetime, timedelta
from sqlalchemy import and_
from .utilities import (
    change_filename,
    comparePic,
    compareText,
    timeEndNotify,
    registreminder,
    examReminder,
    act_with_state,
)
import os
from ..decorator import admin_required, token_required


@manage.route("/deleteactivity", methods=["POST"])
@token_required
@admin_required  # 自定义的权限限制
def deleteactivity(current_user):
    data = request.get_json()
    # print(data)
    # if current_user.author != 1 and current_user.author != 2:
    #     return jsonify(code=RET.DBERR, msg="当前用户没有删除德育活动的权限")
    activity_id = data.get("activity_id")
    try:
        activity = Activity.query.filter_by(id=activity_id).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="查询要删除的德育活动失败")
    # activity = Activity.query.filter_by(id=activity_id).first()
    if activity is None:
        return jsonify(code=RET.DBERR, msg="要删除的德育活动不存在")
    # print(activity)
    parti = Participate.query.filter_by(activity_id=activity_id).all()
    if parti:
        try:
            for p in parti:
                db.session.delete(p)
            db.session.commit()
        except Exception as e:
            current_app.logger.debug(e)
            db.session.rollback()  # commit失败需要回滚
            return jsonify(code=RET.DBERR, msg="删除关联对象失败")
    temprights = TempRight.query.filter_by(activity_id=activity_id).all()
    if temprights:
        try:
            for p in temprights:
                db.session.delete(p)
            db.session.commit()
        except Exception as e:
            current_app.logger.debug(e)
            db.session.rollback()  # commit失败需要回滚
            return jsonify(code=RET.DBERR, msg="删除关联对象失败")
    try:
        db.session.delete(activity)
        db.session.commit()

    except Exception as e:
        current_app.logger.debug(e)
        db.session.rollback()  # commit失败需要回滚
        return jsonify(code=RET.DBERR, msg="删除德育活动失败")
    return jsonify(code=RET.OK, msg="删除德育活动成功")


# 发布德育活动
@manage.route("/createAct", methods=["POST"])
@token_required
@admin_required  # 自定义的权限限制
def createAct(current_user):
    data = request.get_json()
    # print(data)  # debug
    topic = data.get("topic")
    content = data.get("content")
    stime = data.get("stime")
    etime = data.get("etime")
    academy = data.get("academy")
    score = data.get("score")
    nums = data.get("nums")
    try:
        if stime:
            stime = datetime.strptime(stime, "%Y-%m-%d %H:%M:%S")
        if etime:
            etime = datetime.strptime(etime, "%Y-%m-%d %H:%M:%S")
    except Exception:
        return jsonify(code=RET.PARAMERR, msg="时间格式错误")

    try:
        cid = College.query.filter_by(name=academy).first()
        if cid is None:
            return jsonify(code=RET.DATANOTEXIST, msg="学院不存在！")
        else:
            cid = cid.id
        newact = Activity(
            topic=topic,
            content=content,
            startTime=stime,
            endTime=etime,
            score=score,
            college_id=cid,
            maximum=nums,
        )
        db.session.add(newact)
        db.session.flush()
        db.session.commit()
    except Exception:
        db.session.rollback()
        return jsonify(code=RET.DBERR, msg="数据库操作错误")
    # print("########", newact.id)  ### debug
    # print(etime - timedelta(hours=1))  ### debug
    scheduler.add_job(
        id=str(newact.id),
        func=timeEndNotify,
        args=(newact.id,),
        trigger="date",
        run_date=etime - timedelta(hours=1),
        replace_existing=True,
    )

    scheduler.add_job(
        id=str(newact.id + 100000),  # 暂定如此
        func=examReminder,
        args=(newact.id,),
        trigger="date",
        run_date=etime,
        replace_existing=True,
    )

    registreminder(newact.id)
    return jsonify(code=RET.OK, msg="活动创建成功")


# 修改德育活动
@manage.route("/changeAct", methods=["POST"])
@token_required
@admin_required  # 自定义的权限限制
def changeAct(current_user):
    data = request.get_json()
    aid = data.get("aid")
    topic = data.get("topic")
    content = data.get("content")
    stime = data.get("stime")
    etime = data.get("etime")
    academy = data.get("academy")
    score = data.get("score")
    nums = data.get("nums")
    try:
        if stime:
            stime = datetime.strptime(stime, "%Y-%m-%d %H:%M:%S")
        if etime:
            etime = datetime.strptime(etime, "%Y-%m-%d %H:%M:%S")
    except:
        return jsonify(code=RET.PARAMERR, msg="时间格式错误")

    try:
        activity = Activity.query.filter_by(id=aid).first()
    except:
        return jsonify(code=RET.DATANOTEXIST, msg="该id的活动不存在")

    activity.topic = topic
    activity.content = content
    activity.startTime = stime
    activity.endTime = etime
    activity.score = score
    activity.maximum = nums

    try:
        cid = College.query.filter_by(name=academy).first()
        if cid is None:
            return jsonify(code=RET.DATANOTEXIST, msg="学院不存在！")
        else:
            cid = cid.id
        activity.college_id = cid
        db.session.merge(activity)
        db.session.commit()
        return jsonify(code=RET.OK, msg="活动修改成功")
    except:
        db.session.rollback()
    return jsonify(code=RET.DBERR, msg="数据库操作错误")


@manage.route("/signup", methods=["POST"])
@token_required
def signup(current_user):
    data = request.get_json()
    print(data)
    activity_id = data.get("activity_id")
    print(activity_id)
    user = current_user
    try:
        activity = Activity.query.filter_by(id=activity_id).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="查询要报名的德育活动失败")
    if activity is None:
        return jsonify(code=RET.DBERR, msg="要报名的德育活动不存在")
    if user.college_id != activity.college_id:
        return jsonify(code=RET.DBERR, msg="德育活动非本学院举办，不符合报名条件")
    if datetime.now() < activity.startTime:
        return jsonify(code=RET.DBERR, msg="活动未开始")
    if datetime.now() > activity.endTime:
        return jsonify(code=RET.DBERR, msg="活动已结束")

    print(type(activity.users))
    print(len(activity.users))
    if len(activity.users) >= activity.maximum:
        return jsonify(code=RET.DBERR, msg="当前德育活动报名人数已满")

    try:
        parti = Participate(state=0)
        parti.activity = activity
        with db.session.no_autoflush:
            user.activities.append(parti)
            db.session.flush()
        db.session.commit()
    except Exception as e:
        current_app.logger.debug(e)
        db.session.rollback()  # commit失败需要回滚
        return jsonify(code=RET.DBERR, msg="报名德育活动失败")
    return jsonify(code=RET.OK, msg="报名德育活动成功")


@manage.route("/actres", methods=["POST"])
@token_required
def actres(current_user):
    file_dir = os.path.join(os.getcwd(), "app/files/img")
    if not os.path.exists(file_dir):
        os.makedirs(file_dir)
    data = request.form
    print(data)  # debug
    aid = data.get("aid")
    # uid = data.get('uid')
    location = data.get("location")
    text = data.get("text")

    # newf = Feedback.query.filter_by(and_(Feedback.user_id == int(uid), Feedback.activity_id == int(aid))).first()
    newf = (
        db.session.query(Feedback)
        .filter(
            and_(
                Feedback.user_id == int(current_user.id),
                Feedback.activity_id == int(aid),
            )
        )
        .first()
    )
    if newf is not None:
        return jsonify(code=RET.DATAEXIST, msg="该用户对该活动已反馈！")

    pic = request.files.get("file")
    if pic:
        pic_name = change_filename(pic.filename, str(current_user.id) + "_" + str(aid))
        pic_path = os.path.join(file_dir, pic_name)
        pic.save(pic_path)
        comp_pic_ret = comparePic(pic_name, aid)
    else:
        pic_name = None
        comp_pic_ret = 0

    comp_text_ret = compareText(text, aid)

    act = Activity.query.filter_by(id=aid).first()
    if act.endTime < datetime.now():
        return jsonify(code=RET.TIMEOUT, msg="已经超过反馈时间")
    try:
        newf = Feedback(
            user_id=int(current_user.id),
            activity_id=int(aid),
            location=location,
            text=text,
            feedback_time=datetime.now(),  # .strftime("%Y-%m-%d %H:%M:%S")
            pic=pic_name,
            is_copy=(comp_pic_ret == 1 and comp_text_ret == 1),
        )
        db.session.add(newf)
        db.session.commit()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库操作错误")
    print("pic", comp_pic_ret, "text", comp_text_ret)
    if comp_pic_ret == 1 and comp_text_ret == 1:
        return jsonify(code=RET.COPY, msg="反馈雷同")
    return jsonify(code=RET.OK, msg="反馈成功")


@manage.route("/getAct", methods=["POST"])
@token_required
def getAct(current_user):
    data = request.get_json()
    aid = data.get("aid")
    try:
        activity = Participate.query.filter(
            and_(
                Participate.activity_id == aid, Participate.user_id == current_user.id,
            )
        ).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库查询错误")

    if activity is None:
        return jsonify(code=RET.DATANOTEXIST, msg="不存在该报名信息")

    activity_dict = act_with_state(activity)

    return jsonify(code=RET.OK, msg="获取活动详情成功", data=activity_dict)


# @manage.route("/test", methods=["POST"])
# @login_required
# def test():
#     file_dir = os.path.join(os.getcwd(), "app/files/img")
#     data = request.get_json()
#     print(data)  # debug
#     aid = data.get("aid")
#     # uid = data.get('uid')
#     location = data.get("location")
#     text = data.get("text")

#     newf = (
#         db.session.query(Feedback)
#         .filter(
#             and_(
#                 Feedback.user_id == int(current_user.id),
#                 Feedback.activity_id == int(aid),
#             )
#         )
#         .first()
#     )
#     if newf is not None:
#         return jsonify(code=RET.DATAEXIST, msg="该用户对该活动已反馈！")

#     pic = request.files.get("file")
#     if pic:
#         pic_name = change_filename(pic.filename, str(current_user.id) + "_" + str(1))
#         pic_path = os.path.join(file_dir, pic_name)
#         pic.save(pic_path)
#     else:
#         pic_name = None

#     try:
#         newf = Feedback(
#             user_id=int(current_user.id),
#             activity_id=int(aid),
#             location=location,
#             text=text,
#             feedback_time=datetime.now(),  # .strftime("%Y-%m-%d %H:%M:%S")
#             pic=pic_name,
#         )
#         db.session.add(newf)
#         db.session.commit()
#     except Exception as e:
#         current_app.logger.debug(e)
#         return jsonify(code=RET.DBERR, msg="数据库操作错误")
#     return jsonify(code=RET.OK, msg="反馈成功")
