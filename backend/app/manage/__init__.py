# -*- coding =utf-8 -*-
# @Time : 2022/5/7 14:29
# @Author : bk
# @File : __init__.py.py
# @Software : PyCharm

from flask import Blueprint

manage = Blueprint("manage", __name__)

from . import views
