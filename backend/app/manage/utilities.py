import os
from flask import current_app, render_template
from PIL import Image
import imagehash
from datetime import datetime

from .. import db, scheduler
from ..models import Participate, Notice, Feedback, User, Activity
from ..email import send_email


def change_filename(old_name, new_name):
    filename, extension = os.path.splitext(old_name)
    # print(extension)
    return new_name + extension


def comparePic(pic_name, aid):
    file_dir = os.path.join(os.getcwd(), "app/files/img")
    try:
        image1 = Image.open(os.path.join(file_dir, pic_name))
        hash1 = imagehash.average_hash(image1)
    except Exception as e:
        current_app.logger.debug(e)
        return -1
    feedbacks = Feedback.query.filter_by(activity_id=aid).all()
    for feedback in feedbacks:
        try:
            image2 = Image.open(os.path.join(file_dir, feedback.pic))
            hash2 = imagehash.average_hash(image2)
        except Exception as e:
            current_app.logger.debug(e)
        else:
            if hash1 == hash2:
                return 1
    return 0


def compareText(text, aid):
    feedbacks = Feedback.query.filter_by(activity_id=aid).all()
    for feedback in feedbacks:
        if text == feedback.text:
            return 1
    return 0


def timeEndNotify(aid):
    # print("进行通知~", a, b)
    # id_partis = [parti.user.id for parti in partis]
    print(aid)  # debug
    with scheduler.app.app_context():
        # partis = Participate.query.filter_by(activity_id=aid).all()
        partis = Participate.query.filter_by(activity_id=aid).all()
        feedbacks = Feedback.query.filter_by(activity_id=aid).all()
        id_feedbacks = [feedback.user_id for feedback in feedbacks]
        for parti in partis:
            # 寻找报名了没反馈的！
            u = parti.user
            if u.id in id_feedbacks:
                continue
            print(u.username, u.email)  # debug
            act = parti.activity
            send_email(
                u.email,
                "德育活动反馈提醒",
                "mail/time_end_notify",
                username=u.username,
                act_name=act.topic,
                time=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            )
            try:
                notice = Notice(user_id=u.id, notice_type=1, notice_time=datetime.now())
                db.session.add(notice)
                db.session.commit()
            except Exception as e:
                print("###############error!")  # debug
                current_app.logger.debug(e)
                db.session.rollback()  # commit失败需要回滚
        # send_email(to, subject, template)
    # scheduler.remove_job(str(aid))


def examReminder(aid):
    with scheduler.app.app_context():
        admins = User.query.filter(User.author != 3).all()
        act = Activity.query.filter(Activity.id == aid).first()
        for admin in admins:
            send_email(
                admin.email,
                "德育活动审核提醒",
                "mail/exam_reminder",
                username=admin.username,
                act_name=act.topic,
                time=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            )
            try:
                notice = Notice(
                    user_id=admin.id, notice_type=4, notice_time=datetime.now()
                )
                db.session.add(notice)
                db.session.commit()
            except Exception as e:
                print("###############error!")  # debug
                current_app.logger.debug(e)
                db.session.rollback()  # commit失败需要回滚


def registreminder(aid):
    topic = Activity.query.filter_by(id=aid).first().topic
    content1 = "您当前德育分未满60，有新的德育活动发布啦！"
    content2 = "您当前德育分未满80，有新的德育活动发布啦！"
    try:
        users60 = User.query.filter(User.score < 60).all()
        users6080 = User.query.filter(User.score >= 60, User.score < 80).all()
        # users80=User.query.filter(User.score>=80).all()

    except Exception as e:
        current_app.logger.debug(e)
        # return jsonify(code=RET.DBERR, msg="查询未满60分和已满60分但未满80分的用户失败")

    for u in users60:
        print(u)
        # 发送邮件提醒
        send_email(
            u.email,
            "德育活动报名提醒",
            "mail/registration_reminder_60",
            username=u.username,
            score=u.score,
            topic=topic,
            time=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        )

        # 系统内通知提醒
        try:
            notice = Notice(
                user_id=u.id,
                notice_type=2,
                content=content1,
                notice_time=datetime.now(),
            )
            db.session.add(notice)
            db.session.commit()
        except Exception as e:
            current_app.logger.debug(e)
            db.session.rollback()  # commit失败需要回滚

    for u in users6080:
        # print(u)
        send_email(
            u.email,
            "德育活动报名提醒",
            "mail/registration_reminder_6080",
            username=u.username,
            score=u.score,
            topic=topic,
            time=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        )
        # 系统内通知提醒
        try:
            notice = Notice(
                user_id=u.id,
                notice_type=2,
                content=content2,
                notice_time=datetime.now(),
            )
            db.session.add(notice)
            db.session.commit()
        except Exception as e:
            current_app.logger.debug(e)
            db.session.rollback()  # commit失败需要回滚
    # print(users60,users6080,users80)
    print("活动报名提醒成功")


def act_with_state(activity):
    ret = {}
    ret["aid"] = activity.activity_id
    ret["topic"] = activity.activity.topic
    ret["stime"] = activity.activity.startTime.strftime("%Y-%m-%d %H:%M:%S")
    ret["etime"] = activity.activity.endTime.strftime("%Y-%m-%d %H:%M:%S")
    ret["academy"] = activity.activity.college.name
    ret["score"] = activity.activity.score
    ret["nums"] = activity.activity.maximum
    ret["content"] = activity.activity.content
    ret["status"] = activity.state

    return ret
