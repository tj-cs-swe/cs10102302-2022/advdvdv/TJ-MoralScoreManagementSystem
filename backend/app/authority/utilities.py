from flask import jsonify, request, current_app
from ..models import College, TempRight, User,Activity
from ..response import RET

def manager_right(user):
    keys = ("id", "username","author")
    ret = {}

    for key in keys:
        ret[key] = user.__getitem__(key)
    return ret

def guidance_right(user):
    keys = ("id", "username","author")
    ret = {}

    for key in keys:
        ret[key] = user.__getitem__(key)
    return ret

def ordinary_right(user):
    keys = ("id", "username","author")
    ret = {}
    rights=TempRight.query.filter_by(user_id=user.id).all()
    
    activities=[]
    for right in rights:
        activities.append(Activity.query.filter_by(id=right.activity_id).first().topic)
    ret["activities"]=activities
    for key in keys:
        ret[key] = user.__getitem__(key)
    return ret

def user_right(user):
    keys = ("author","no","username","college_name","id")
    rights_name=["管理员","辅导员","普通用户","临时辅导员"]
    ret = {}    
    for key in keys:
        if key=="college_name":
            try:
                college=College.query.filter_by(id=user.college_id).first()
            except Exception as e:
                current_app.logger.debug(e)
            if college is None:
                ret[key]="NULL"
            else:
                ret[key]=college.__getitem__("name")
        elif key=="author":
            ret[key]=rights_name[user.author-1]
            rights=TempRight.query.filter_by(user_id=user.id).all()
            if user.author==3 and len(rights)>0:
                ret[key]=rights_name[3]
        else:
            ret[key] = user.__getitem__(key)
    return ret