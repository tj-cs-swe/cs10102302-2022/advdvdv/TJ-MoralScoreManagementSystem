from flask import jsonify, request, current_app
from . import authority
from .. import db
from ..models import User, TempRight, Activity
from ..response import RET
from .utilities import user_right
from ..decorator import admin_required, token_required
from datetime import datetime
from sqlalchemy import and_


@authority.route("/change", methods=["POST"])
@token_required
@admin_required  # 自定义的权限限制
def change(current_user):
    data = request.get_json()
    uid = data.get("uid")
    anum = data.get("authority")

    u = User.query.filter_by(id=uid).first()
    u.author = anum

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库错误")

    return jsonify(code=RET.OK, msg="更改成功")


@authority.route("/deltemp", methods=["POST"])
@token_required
@admin_required  # 自定义的权限限制
def deltemp(current_user):
    data = request.get_json()
    uid = data.get("uid")
    aid = data.get("aid")
    try:
        right = TempRight.query.filter_by(user_id=uid, activity_id=aid).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库查询失败")
    if right:
        try:
            db.session.delete(right)
            db.session.commit()
        except Exception as e:
            current_app.logger.debug(e)
            db.session.rollback()  # commit失败需要回滚
            return jsonify(code=RET.DBERR, msg="数据库删除失败")
        return jsonify(code=RET.OK, msg="删除临时权限成功")
    return jsonify(code=RET.DATANOTEXIST, msg="不存在该临时权限")


@authority.route("/addtemp", methods=["POST"])
@token_required
@admin_required  # 自定义的权限限制
def addtemp(current_user):
    data = request.get_json()
    uid = data.get("uid")
    aid = data.get("aid")
    try:
        right = TempRight.query.filter_by(user_id=uid, activity_id=aid).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库查询失败")
    if right:
        return jsonify(code=RET.DATAEXIST, msg="权限已存在")
    try:
        tempright = TempRight(user_id=uid, activity_id=aid)
        db.session.add(tempright)
        db.session.commit()
    # u = User.query.filter_by(id=uid).first()
    except Exception as e:
        current_app.logger.debug(e)
        db.session.rollback()  # commit失败需要回滚
        return jsonify(code=RET.DBERR, msg="数据添加失败")
    return jsonify(code=RET.OK, msg="添加权限成功")


@authority.route("/alluser", methods=["POST"])
@token_required
@admin_required  # 自定义的权限限制
def viewright(current_user):
    try:
        # manager = User.query.filter_by(author=1).all()
        # guidance = User.query.filter_by(author=2).all()
        # ordinary = User.query.filter_by(author=3).all()
        users=User.query.all()

    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库查询失败")

    return jsonify(code=RET.OK, msg="查看所有权限成功", data=list(map(user_right, users)))


@authority.route("/activities_available", methods=["POST"])
@token_required
def activities_available(current_user):
    try:
        activities = (
            Activity.query.filter(Activity.endTime > datetime.now())
            .order_by(Activity.startTime)
            .all()
        )

    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库查询错误")

    return jsonify(code=RET.OK, msg="获取全部活动成功", activities=list(map(dict, activities)),)


@authority.route("/activities_with_authorities", methods=["POST"])
@token_required
def activities_with_authorities(current_user):
    data = request.get_json()
    uid = data.get("uid")
    activities_list = []

    try:
        tempright = TempRight.query.filter(TempRight.user_id == uid).all()
        for r in tempright:
            activities_list.append(
                Activity.query.filter(Activity.id == r.activity_id).first()
            )

    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="数据库查询错误")

    return jsonify(
        code=RET.DBERR,
        msg="获取权限活动成功",
        activities_list=list(map(dict, activities_list)),
    )


@authority.route("/single_ser_authority", methods=["POST"])
@token_required
def single_ser_authority(current_user):
    data = request.get_json()
    aid = data.get("aid")

    try:
        tempright = TempRight.query.filter(
            and_(TempRight.user_id == current_user.id, TempRight.activity_id == aid,)
        ).first()
    except Exception as e:
        current_app.logger.debug(e)
        return jsonify(code=RET.DBERR, msg="查询权限失败")
    if tempright is not None:
        return jsonify(code=RET.OK, msg="用户拥有该权限")
    else:
        return jsonify(code=RET.AUTHERR, msg="用户没有此权限")
