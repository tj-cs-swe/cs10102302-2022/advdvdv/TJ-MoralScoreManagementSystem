import click
import datetime
from app import app, db
from .models import College, User, Activity, Participate, Feedback, Notice, TempRight


@app.cli.command()
@click.argument("test_names", nargs=-1)
def test(test_names):
    """Run the unit tests."""
    import unittest

    if test_names:
        tests = unittest.TestLoader().loadTestsFromNames(test_names)
    else:
        tests = unittest.TestLoader().discover("tests")
    unittest.TextTestRunner(verbosity=2).run(tests)


@app.cli.command()
def db_init():
    """创建数据库"""
    db.drop_all()
    db.create_all()

    user1 = User(
        email="user1@qq.com",
        # tel="123456789",
        username="user1",
        password="123",
        no="1953001",
        score=5,
        author=1,
        college_id=1,
    )
    user2 = User(
        email="3254909065@qq.com",
        # tel="123456789",
        username="user2",
        password="123",
        no="1953111",
        score=65,
        author=2,
    )
    user3 = User(
        email="renxin-ty@qq.com",
        # tel="123456789",
        username="user3",
        password="123",
        no="1952001",
        score=85,
        author=3,
    )
    user4 = User(
        email="user4@qq.com",
        # tel="123456789",
        username="user4",
        password="123",
        no="1954001",
        score=65,
        author=3,
    )

    user5 = User(
        email="user5@qq.com",
        # tel="123456789",
        username="user5",
        password="123",
        no="1955001",
        score=70,
        author=3,
    )

    activity1 = Activity(
        topic="test1",
        content="yk我爱你！",
        startTime=datetime.datetime(2022, 5, 1, 18, 0, 0),
        endTime=datetime.datetime(2022, 5, 30, 19, 30, 0),
        score=5,
        college_id=1,
        maximum=2,
    )
    activity2 = Activity(
        topic="test2",
        content="yk我爱你！！",
        startTime=datetime.datetime(2022, 5, 30, 18, 0, 0),
        endTime=datetime.datetime(2022, 6, 1, 19, 30, 0),
        score=10,
        college_id=2,
        maximum=100,
    )
    activity3 = Activity(
        topic="test3",
        content="yk我爱你！！！",
        startTime=datetime.datetime(2022, 6, 1, 18, 0, 0),
        endTime=datetime.datetime(2022, 6, 8, 19, 30, 0),
        score=15,
        college_id=1,
        maximum=100,
    )
    activity4 = Activity(
        topic="test4",
        content="yk我爱你！！！！",
        startTime=datetime.datetime(2022, 5, 1, 18, 0, 0),
        endTime=datetime.datetime(2022, 5, 10, 19, 30, 0),
        score=20,
        college_id=1,
        maximum=100,
    )
    activity5 = Activity(
        topic="test5",
        content="yk我爱你！！！！！",
        startTime=datetime.datetime(2022, 5, 1, 18, 0, 0),
        endTime=datetime.datetime(2022, 6, 1, 19, 30, 0),
        score=25,
        college_id=1,
        maximum=100,
    )
    activity6 = Activity(
        topic="test6",
        content="我们一起写软工，一起啊对对对对",
        startTime=datetime.datetime(2022, 5, 1, 18, 0, 0),
        endTime=datetime.datetime(2022, 6, 1, 19, 30, 0),
        score=5,
        college_id=1,
        maximum=100,
    )
    activity7 = Activity(
        topic="test7",
        content="我们一起写软工，一起啊对对对对",
        startTime=datetime.datetime(2022, 5, 1, 18, 0, 0),
        endTime=datetime.datetime(2022, 6, 1, 19, 30, 0),
        score=5,
        college_id=1,
        maximum=100,
    )
    activity8 = Activity(
        topic="test8",
        content="我们一起写软工，一起啊对对对对",
        startTime=datetime.datetime(2022, 5, 1, 18, 0, 0),
        endTime=datetime.datetime(2022, 6, 1, 19, 30, 0),
        score=5,
        college_id=1,
        maximum=100,
    )
    activity9 = Activity(
        topic="test9",
        content="我们一起写软工，一起啊对对对对",
        startTime=datetime.datetime(2022, 5, 1, 18, 0, 0),
        endTime=datetime.datetime(2022, 6, 1, 19, 30, 0),
        score=5,
        college_id=1,
        maximum=100,
    )
    activity10 = Activity(
        topic="test10",
        content="我们一起写软工，一起啊对对对对",
        startTime=datetime.datetime(2022, 5, 1, 18, 0, 0),
        endTime=datetime.datetime(2022, 6, 1, 19, 30, 0),
        score=5,
        college_id=1,
        maximum=100,
    )
    activity11 = Activity(
        topic="test11",
        content="我们一起写软工，一起啊对对对对",
        startTime=datetime.datetime(2022, 5, 1, 18, 0, 0),
        endTime=datetime.datetime(2022, 6, 1, 19, 30, 0),
        score=5,
        college_id=1,
        maximum=100,
    )

    feedback1 = Feedback(
        user_id=1,
        activity_id=1,
        location="地点1",
        text="文字1",
        feedback_time=datetime.datetime(
            2022, 5, 1, 18, 0, 0
        ),  # .strftime("%Y-%m-%d %H:%M:%S")
        pic="1_1.jpg",
    )
    feedback2 = Feedback(
        user_id=1,
        activity_id=2,
        location="地点2",
        text="文字2",
        feedback_time=datetime.datetime(
            2022, 5, 1, 18, 0, 0
        ),  # .strftime("%Y-%m-%d %H:%M:%S")
        pic="1_2.jpg",
    )

    feedback3 = Feedback(
        user_id=2,
        activity_id=3,
        location="地点3",
        text="文字3",
        feedback_time=datetime.datetime(
            2022, 5, 1, 18, 0, 0
        ),  # .strftime("%Y-%m-%d %H:%M:%S")
        pic="2_3.jpg",
    )

    # participate1 = Participate(user_id=1, activity_id=1)
    # participate2 = Participate(user_id=2, activity_id=1)
    # participate3 = Participate(user_id=1, activity_id=2)

    notice1 = Notice(
        user_id=1, notice_type=1, notice_time=datetime.datetime(2022, 5, 1, 18, 0, 0)
    )

    notice2 = Notice(
        user_id=1, notice_type=2, notice_time=datetime.datetime(2022, 5, 1, 18, 1, 0)
    )

    notice3 = Notice(
        user_id=2, notice_type=3, notice_time=datetime.datetime(2022, 5, 1, 18, 0, 0)
    )

    tempright1 = TempRight(user_id=3, activity_id=1)

    tempright2 = TempRight(user_id=3, activity_id=2)

    tempright3 = TempRight(user_id=4, activity_id=5)

    college0 = College(name="未绑定", id=0)
    college1 = College(name="电子与信息工程学院", bindCode="dxxy")
    college2 = College(name="交通学院", bindCode="jtxy")
    college3 = College(name="汽车学院", bindCode="qcxy")
    college4 = College(name="软件学院", bindCode="rjxy")
    college5 = College(name="艺术与传媒学院", bindCode="ycxy")
    try:
        db.session.add(college0)
        db.session.add_all(
            [
                user1,
                user2,
                user3,
                user4,
                user5,
                activity1,
                activity2,
                activity3,
                activity4,
                activity5,
                activity6,
                activity7,
                activity8,
                activity9,
                activity10,
                activity11,
                feedback1,
                feedback2,
                feedback3,
                notice1,
                notice2,
                notice3,
                tempright1,
                tempright2,
                tempright3,
                college1,
                college2,
                college3,
                college4,
                college5
                # participate1,
                # participate2,
                # participate3,
            ]
        )
        parti1 = Participate(state=2)
        parti1.activity = activity1
        user1.activities.append(parti1)

        parti2 = Participate(state=1)
        parti2.activity = activity1
        user2.activities.append(parti2)

        parti3 = Participate(state=0)
        parti3.activity = activity2
        user1.activities.append(parti3)

        db.session.commit()
    except Exception as e:
        db.session.rollback()
        click.echo(e)
    else:
        click.echo("创建成功")
