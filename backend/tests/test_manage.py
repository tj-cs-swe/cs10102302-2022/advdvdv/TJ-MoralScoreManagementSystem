import unittest
from app import create_app, db
from app.models import User,Activity,Participate
from datetime import datetime

class BrowseTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app("testing")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    
    #测试已存在的德育活动可以正常报名
    def test_signup(self):
        u1 = User(id=1,college_id=2)
        act1=Activity(id=1)
        db.session.add_all([u1,act1])
        activity_id=1
        activity=Activity.query.filter_by(id=activity_id).first()
        user=User.query.filter_by(id=1).first()

        p=Participate(state=1)
        p.activity=activity
        with db.session.no_autoflush:
            user.activities.append(p)
            db.session.flush()
        db.session.commit()
    
    #测试不存在的德育活动不可以报名
    def test_no_signup(self):
        u2 = User(id=2)
        act2=Activity(id=2)
        db.session.add_all([u2,act2])
        db.session.commit()
        activity_id=10
        #with self.assertRaises(AttributeError):
        activity=Activity.query.filter_by(id=activity_id).first()
        user=User.query.filter_by(id=1).first()

    #测试不是本学院举行的活动不可以报名
    def test_college_id(self):
        u3 = User(id=3)
        act3=Activity(id=3)

    #测试活动未开始不可以报名
    def test_start_time(self):
        u4 = User(id=4)
        act4=Activity(id=4,startTime=datetime(2022, 10, 30, 18, 0, 0))
        db.session.add_all([u4,act4])
        db.session.commit()
        activity_id=4
        activity=Activity.query.filter_by(id=activity_id).first()
        user=User.query.filter_by(id=5).first()
        if datetime.now()<activity.startTime:
            self.assertTrue(datetime.now()<activity.startTime)


    #测试不是已结束的活动不可以报名
    def test_end_time(self):
        u5 = User(id=5)
        act5=Activity(id=5,endTime=datetime(2022, 5, 1, 18, 0, 0))
        db.session.add_all([u5,act5])
        db.session.commit()
        activity_id=5
        activity=Activity.query.filter_by(id=activity_id).first()
        user=User.query.filter_by(id=5).first()
        if datetime.now()>activity.endTime:
            self.assertTrue(datetime.now()>activity.endTime)
