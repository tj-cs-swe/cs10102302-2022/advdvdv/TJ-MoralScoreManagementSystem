import unittest
from app import create_app, db
from app.models import Activity, User
import datetime

class UserModelTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()
    
    #测试活动结束时间晚于活动开始时间可以设置成功
    def test_timeset(self):
        activity=Activity(id=1,startTime=datetime.datetime(2022, 1, 17, 22, 0, 0),endTime=datetime.datetime(2022, 2, 17, 22, 0, 0))



    #测试活动主题字段长度不可以超过64
    def test_topic_len(self):
        activity=Activity(id=1,topic="学术之星")
        self.assertTrue(len(activity.topic)<64)
        # db.session.add(activity)
        # db.session.commit()


